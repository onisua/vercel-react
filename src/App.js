import React from "react";
import "./App.css";

function App() {
  const authorPseudo = "a4h8 ";
  const twitterUrl = "https://twitter.com/onisua";
  const repo_Url = "https://onisua@bitbucket.org/onisua/vercel-react.git";

  const reactLogo = "/logo192.png";
  return (
    <div className="app">
      <a href="/" title="React Logo">
        <img src={reactLogo} alt="React Logo" />
      </a>
      <h1>React App on Vercel</h1>
      <p>A Simple React App Deployed on Vercel</p>

      <div className="app_container">
        <div className="list">
          <h2 className="list_item">Create react app</h2>

          <p>A Simple React App Deployed on Vercel</p>
        </div>
        <div className="list">
          <h2 className="list_item">Git account Intergration</h2>
          <p>A Simple React App Deployed on Vercel</p>
        </div>
        <div className="list">
          <h2 className="list_item">Setup Your React App</h2>
          <p>
            <code>Coming Soon</code>
          </p>
        </div>
        <div className="list">
          <h2 className="list_item">Setup Your API</h2>

          <p>Coming Soon</p>
        </div>
        <div className="copy">
          <div className="icon">
            <a
              href={repo_Url}
              title="Fork this project repository"
              target="_blank"
            >
              <svg
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M7 5C7 3.89543 7.89543 3 9 3C10.1046 3 11 3.89543 11 5C11 5.74028 10.5978 6.38663 10 6.73244V14.0396H11.7915C12.8961 14.0396 13.7915 13.1441 13.7915 12.0396V10.7838C13.1823 10.4411 12.7708 9.78837 12.7708 9.03955C12.7708 7.93498 13.6662 7.03955 14.7708 7.03955C15.8753 7.03955 16.7708 7.93498 16.7708 9.03955C16.7708 9.77123 16.3778 10.4111 15.7915 10.7598V12.0396C15.7915 14.2487 14.0006 16.0396 11.7915 16.0396H10V17.2676C10.5978 17.6134 11 18.2597 11 19C11 20.1046 10.1046 21 9 21C7.89543 21 7 20.1046 7 19C7 18.2597 7.4022 17.6134 8 17.2676V6.73244C7.4022 6.38663 7 5.74028 7 5Z"
                  fill="currentColor"
                />
              </svg>
            </a>
          </div>

          <p>
            Made by {}
            <a
              href={twitterUrl}
              target="_blank"
              title={`Follow @${authorPseudo} on twitter`}
            >
              @{authorPseudo}
            </a>
            with ☕
          </p>
        </div>
      </div>
    </div>
  );
}

export default App;
